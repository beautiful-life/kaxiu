package com.kaxiu.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kaxiu.persistent.entity.SysDic;
import com.kaxiu.persistent.mapper.SysDicMapper;
import com.kaxiu.service.ISysDicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class SysDicServiceImpl extends ServiceImpl<SysDicMapper, SysDic> implements ISysDicService {

    @Autowired
    private SysDicMapper mapper;

    @Override
    public List<SysDic> getDicByArr(String[] dics) {
        return mapper.selectList(Wrappers.<SysDic>lambdaQuery()
                .eq(SysDic::getDeleted, "0")
                .eq(SysDic::getStatus, "1")
                .in(SysDic::getDicKey, dics)
        );
    }
}
