package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.CarInfo;
import com.kaxiu.persistent.mapper.CarInfoMapper;
import com.kaxiu.service.ICarInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 车辆信息
 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class CarInfoServiceImpl extends ServiceImpl<CarInfoMapper, CarInfo> implements ICarInfoService {

}
