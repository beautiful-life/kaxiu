package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.BasicUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户信息 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface BasicUserMapper extends BaseMapper<BasicUser> {

}
