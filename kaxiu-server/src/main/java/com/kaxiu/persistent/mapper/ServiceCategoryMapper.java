package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.ServiceCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 维修类别(商品)表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface ServiceCategoryMapper extends BaseMapper<ServiceCategory> {

    List<ServiceCategory> selectByParentId(@Param("id") Serializable id);

    List<ServiceCategory> selectAllCategory();

}
