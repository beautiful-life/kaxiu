package com.kaxiu.persistent.entity;

import java.math.BigDecimal;
import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 资金账户表
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Account extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 可用金额
     */
    private BigDecimal amount;

    /**
     * 冻结金额
     */
    private BigDecimal frozenMount;

    /**
     * 已提现金额
     */
    private BigDecimal withdraw;

    /**
     * 账户类型: 1:个人账户
     */
    private Integer type;


}
