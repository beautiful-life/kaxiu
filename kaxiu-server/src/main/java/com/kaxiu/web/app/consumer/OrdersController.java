package com.kaxiu.web.app.consumer;


import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.service.IOrdersService;
import com.kaxiu.vo.ResultDataWrap;
import com.kaxiu.vo.order.OrderRequestVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@RestController
@RequestMapping("/kaxiu/orders")
public class OrdersController extends AbstractBaseController {

    @Resource
    private IOrdersService ordersService;

    /**
     * consumer小程序使用接口
     * @param vo
     * @return
     */
    @PostMapping("")
    @RequiresPermissions("consumer:takeOrder")
    public ResultDataWrap takeOrders(@RequestBody OrderRequestVo vo){
        return buildResult(ordersService.takeOrder(vo));
    }

    /**
     * consumer小程序使用接口
     * @param status
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping("/list")
    public ResultDataWrap getOrderList(@RequestParam Integer status,
                                       @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                       @RequestParam(required = false, defaultValue = "20") Integer pageSize){
        return buildResult(ordersService.getOrderDetailByUserId(status, pageSize, pageIndex));
    }

    /**
     * consumer小程序使用接口
     * @param orderId
     * @return
     */
    @GetMapping("/info")
    public ResultDataWrap getOrderList(@RequestParam Serializable orderId){
        return buildResult(ordersService.getOrderDetailById(orderId));
    }

    /**
     * consumer小程序使用接口
     * @param orderId
     * @param rate
     * @param review
     * @return
     */
    @GetMapping("/rate")
    public ResultDataWrap getOrderList(@RequestParam Serializable orderId,
                                       @RequestParam Integer rate,
                                       @RequestParam(required = false) String review){
        return buildResult(ordersService.rateById(orderId, rate, review));
    }



}

